import json
from flask import request, Flask

APP = Flask(__name__)


@APP.route('/plus_one')
def plus_one():
    variable = int(request.args.get('x', 1))
    return json.dumps({'x': variable + 1})


@APP.route('/plus_two')
def plus_two():
    variable = int(request.args.get('x', 1))
    return json.dumps({'x': variable + 2})


@APP.route('/square')
def square():
    variable = int(request.args.get('x', 1))
    return json.dumps({'x': variable * variable})
